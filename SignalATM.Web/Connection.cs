﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace InfoFreightSystem
{
    public class Connection
    {
        #region Member Variable


        /// <summary>
        /// Gets the connection string.
        /// </summary>
        /// <value>The connection string.</value>
        public static string SqlConnectionString
        {
            get
            {
                // return string.Empty;
                return ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString;

            }
        }



        #endregion

        #region Constructor



        #endregion

        #region Member Function


        #endregion
    }
}