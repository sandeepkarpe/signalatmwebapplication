﻿
using Hammock;
using Hammock.Authentication.OAuth;
using Hammock.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OrderWorkAPI.Services;
using RestSharp;
using Sparkle.LinkedInNET;
using Sparkle.LinkedInNET.Profiles;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TweetSharp;
//using System.Net.WebRequestMethods;
using System.Security.Permissions;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Drawing.Imaging;
using System.Dynamic;
using InfoFreightSystem;
using SignalATM.Web.Models;
using System.Data;
using System.Data.SqlTypes;
using System.Xml;

namespace SignalATM.Web.Controllers
{
    //[RequireHttps]
    public class HomeController : Controller
    {        
        String googleplus_client_id = "777572952348-jbfu7g82nvg2r7kveohb3qj43iaibd7l.apps.googleusercontent.com";

        String redirect_url = ConfigurationManager.AppSettings["redirect_url"];
           
        String googleplus_client_secret = "mHONhs0dAKZaj8mewNUB392e";

        string loginType = String.Empty;

        Userclass userAllInfo = new Userclass();
        SQLDataAccess sqlDataAccess;
        Hashtable inputParams;
        SignalATMClass _SignalATM;
        public GlobalVariables globalVaribles;
        public void SetGlobalVariables(string email)
        {
             sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@strApplicationUserLoginName", email );
            globalVaribles = new GlobalVariables();
            DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetGlobalVariablesData", inputParams);
            globalVaribles.SecUserID =Convert.ToInt32(dt.Rows[0]["UserID"]);
            globalVaribles.RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
            globalVaribles.SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
            globalVaribles.RoleName = dt.Rows[0]["RoleName"].ToString();
           
        }
       public ActionResult Login(string emailId)
       {                  
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public string IsFirstLogin(string email)
        {
            Hashtable ht_reg = new Hashtable();
            ht_reg.Add("@ApplicationUserLoginName", email); 
            sqlDataAccess = new SQLDataAccess();          
             var res  = sqlDataAccess.ExecuteStoreProcedure("usp_IsFirstLogin", ht_reg);                  
            return res;
        }
        public string IsCountryAndTimezoneExist(string email)
        {
            Hashtable ht_reg = new Hashtable(); 
            ht_reg.Add("@strApplicationUserLoginName", email);
            sqlDataAccess = new SQLDataAccess();
            var res = sqlDataAccess.ExecuteStoreProcedure("usp_IsCountryAndTimezoneExist", ht_reg);
            return res.ToString();
        }
        public JsonResult LoginDetailsAsync(string email, string password)
        {
            try
            {               
                UserMgnt u = new UserMgnt();
                // string de = u.Decrypt("kX2W3DZioUVMVXDcyNv87HQGBY6QCGfKaklk1y5L+W4=");
               // ViewBag["UserId"] = email;
                Session["UserId"] = email;               
                Hashtable hInput = new Hashtable();
                 string pwd = u.Encrypt(password);
                hInput.Add("@iUserEmail", email);
                hInput.Add("@strApplicationUserLoginPassword", pwd);         
                SignalATMClass signalATM = new SignalATMClass();
                string isUserExists = signalATM.CheckEmailAddress(hInput);
                if (isUserExists != "0")
                {
                    if(isUserExists == "2")
                    {                       
                        var isFirst = IsFirstLogin(email);
                        if (isFirst == "False")
                        {
                            string res = IsCountryAndTimezoneExist(email);
                            if (res == "0")
                            {
                                return Json(new { success = true, result = "BasicConfiguration" }, JsonRequestBehavior.AllowGet);
                            }
                            u.Password = u.Encrypt(password);
                            //   string de = u.Decrypt("7ho0jeq6FwTMvOZOuJPmeQ==");
                            var request = (HttpWebRequest)WebRequest.Create(ConfigurationManager.AppSettings["WebAPI_URL"].ToString() + "token");
                            var postData = "grant_type=" + Uri.EscapeDataString(ConfigurationManager.AppSettings["GrantType"].ToString());
                            postData += "&username=" + Uri.EscapeDataString(email);
                            //postData += "&password=" + Uri.EscapeDataString(txtPassword.Text);
                            postData += "&password=" + Uri.EscapeDataString(u.Password);
                            var data = Encoding.ASCII.GetBytes(postData);
                            SetGlobalVariables(email);
                            request.Method = "POST";
                            request.ContentType = "application/x-www-form-urlencoded";
                            request.ContentLength = data.Length;                                                      
                            using (var stream = request.GetRequestStream())
                            {
                                stream.Write(data, 0, data.Length);
                            }
                            var response = (HttpWebResponse)request.GetResponse();
                            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
                            if (responseString != "")
                            {
                                JObject jObject = JObject.Parse(responseString);
                                string Access_token = (string)jObject.SelectToken("access_token");
                                string token_type = (string)jObject.SelectToken("token_type");
                                string expires_in = (string)jObject.SelectToken("expires_in");
                                string UserName = (string)jObject.SelectToken("UserName");
                                System.Web.HttpCookie myCookie = new System.Web.HttpCookie("access_token");
                                Session["UserName"] = UserName;
                                Session["Access_token"] = Access_token;
                                Session["token_type"] = token_type;
                                Session["expires_in"] = expires_in;
                                int roleId = globalVaribles.RoleID;
                                return Json(new { success = true, result = "OK" + roleId }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {
                                return Json(new { success = true, result = "Failed" }, JsonRequestBehavior.AllowGet);
                            }
                        
                        }
                        else
                        {
                            return Json(new { success = true, result = "Firstlogin" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = true, result = "NotActive" }, JsonRequestBehavior.AllowGet);
                    }                    
                }
                else
                {
                    return Json(new { success = true, result = "NotExist" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                return Json(new { success = true, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult UserNotActiveMessage()
        {
            return View();
        }
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Login", "Home");
        }
        [HttpGet]
        public ActionResult Register()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        [HttpPost]
        public async Task<JsonResult> RegisterDetails(string email, string password)
        {
            try
            {
                otp otp = new otp();
                UserMgnt u = new UserMgnt();
                var genOtp = otp.GenerateOtp();
                u.Password = u.Encrypt(password);
                string hostName = Dns.GetHostName();  
                string ipAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
                Hashtable ht_reg = new Hashtable();
                ht_reg.Add("@strUserEmail", email);
                ht_reg.Add("@strUserPassword", u.Password);
                ht_reg.Add("@iUserOTP", genOtp);
                ht_reg.Add("@iRoleID", 6);
                ht_reg.Add("@strUserIPAddresses", ipAddress);
                ht_reg.Add("@bUserIsActive", 0);
                _SignalATM = new SignalATMClass();
                string strResult = _SignalATM.Register(ht_reg);
                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strApplicationUserEmailID", email);
                var userId = _SignalATM.GetuserId(reg_User);              
                if (strResult != "User already exists.")
                {
                    Session["EmailTo"] = email;
                    Session["OTP"] = strResult;
                    ViewBag.OTP = strResult;

                    EmailSimple emailSimple = new EmailSimple();
                    StringBuilder sb_SendEmail = new StringBuilder();
                    string subject = "User Registration";                    
                    var values = new Dictionary<string, string>();
                    values.Add("mailTo", email);
                    values.Add("mailSubject",subject);
                    var content = new FormUrlEncodedContent(values);

                    JObject relTypes = new JObject();
                    using (var client = new HttpClient())
                    {                                    
                         string uri = ConfigurationManager.AppSettings["DomainName"].ToString() + "/Home/ActivateUserAccount?email=" + email;
                        //client.BaseAddress = new Uri("http://localhost:53743/api/serviceproviders/");
                        client.BaseAddress =new Uri( ConfigurationManager.AppSettings["BaseUrl"].ToString());                        
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage resp = await client.GetAsync("serviceproviders/sendactivationemail?mailTo=" + email+ "&mailSubject="+subject+"&url="+ uri);

                        if (resp.IsSuccessStatusCode)
                        {                           
                            return Json(new { success = true, result = "OK" }, JsonRequestBehavior.AllowGet);
                            //resp.Content.ReadAsStringAsync().Result;
                            //var resvalues = JObject.Parse(Response);                     
                        }
                        else
                        {
                            return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    return Json(new { success = true, result = "False" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch(Exception ex)
            {
                string msg = ex.Message;
                return Json(new { success = true, result = "fail" }, JsonRequestBehavior.AllowGet);
            }
          //  return Json(new { success = true, result = "fail" }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ResendEmail(string email)
        {
            ViewBag.reEmail = email;
            return View();
        }
        public ActionResult SuccessfullRegistrationMsg()
        {
            return View();
        }
        public async Task<ActionResult> SendActivationMail(string email)
        {           
            string subject = "User account activation mail"; 
            using (var client = new HttpClient())
            {
                string uri = ConfigurationManager.AppSettings["DomainName"].ToString() + "/Home/ActivateUserAccount?email=" + email;
               // client.BaseAddress = new Uri("http://localhost:53743/api/serviceproviders/");
               client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.GetAsync("serviceproviders/sendactivationemail?mailTo=" + email + "&mailSubject=" + subject + "&url=" + uri);
                SignalATMClass _SignalATM = new SignalATMClass();
                _SignalATM.UpdateActivationEmailDate(email);
                if (resp.IsSuccessStatusCode)
                {
                    return RedirectToAction("Login","Home");
                    //return Json(new { success = true, result = "OK" }, JsonRequestBehavior.AllowGet);                                    
                }
                else
                {
                    return RedirectToAction("Login", "Home");
                }
               
            }
        }
        public ActionResult ActivateUserAccount()
        {
            string email = Request.QueryString["email"].ToString();
            ViewBag.EmailId = email;
            Hashtable ht_reg = new Hashtable();
            ht_reg.Add("@strApplicationUserLoginName", email);
            sqlDataAccess = new SQLDataAccess();
            string result = sqlDataAccess.ExecuteStoreProcedure("usp_ActiveUser", ht_reg);
            if(result=="0")
            {
                ViewBag.response = "Your account is already activated . Please login with valid account number and password!";
                return View();
            }
            else if(result=="3")
            {
                return RedirectToAction("ResendEmail", "Home",new { email=email });
            }
            else
            {
                return RedirectToAction("Login", "Home");
            }
           
        }       
        public ActionResult Forgot_Password()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Forgot_PasswordDetails(string email)   
        {
            return View();      
        }
        public async Task<JsonResult> SaveForgotPassword(string userEmail, string password)
        {
            if (userEmail != null && password != null)
            {
                UserMgnt u = new UserMgnt();
                u.Password = u.Encrypt(password);
                SignalATMClass _SignalATM = new SignalATMClass();
                Hashtable hs = new Hashtable();
                hs.Add("@email", userEmail);
                hs.Add("@password", u.Password);
                string strResult = _SignalATM.UpdatePassword(hs);
                if (strResult == "True")
                {
                    using (var client = new HttpClient())
                    {
                        string subject = "Forgot Password";
                        string mailBody = "";
                        // client.BaseAddress = new Uri("http://localhost:53743/api/serviceproviders/");
                        client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        HttpResponseMessage resp = await client.GetAsync("serviceproviders/SendPasswordResetEmail?mailTo=" + userEmail + "&mailSubject=" + subject + "&mailBody=" + mailBody);

                        if (resp.IsSuccessStatusCode)
                        {
                            return Json(new { status = true, result = "OK" }, JsonRequestBehavior.AllowGet);
                            //resp.Content.ReadAsStringAsync().Result;
                            //var resvalues = JObject.Parse(Response);                     
                        }
                        else
                        {
                            return Json(new { status = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { success = true, status = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, status = false }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { success = true, status = "" }, JsonRequestBehavior.AllowGet);
        }
        public async Task<JsonResult> IsUserExistsPwd(string email)
        {
            try
            {
                Session["UserId"] = email;
                Hashtable hInput = new Hashtable();
                hInput.Add("@iUserEmail", email);
                SignalATMClass signalATM = new SignalATMClass();
                string isUserExists = signalATM.CheckEmailAddressExists(hInput);
                if (isUserExists != "0")
                {
                    if (isUserExists == "1")
                    {
                        using (var client = new HttpClient())
                        {
                            string subject = "Forgot Password";
                            string url = ConfigurationManager.AppSettings["DomainName"].ToString() + "/Home/Forgot_PasswordDetails?email=" + email;

                            client.BaseAddress = new Uri("http://apps.brainlines.net/SignalATMWebAPI/api/serviceproviders/");
                            client.DefaultRequestHeaders.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                            HttpResponseMessage resp = await client.GetAsync("SendPasswordResetEmaillink?mailTo=" + email + "&mailSubject=" + subject + "&url=" + url);

                            if (resp.IsSuccessStatusCode)
                            {
                                return Json(new { status = true, result = "OK" }, JsonRequestBehavior.AllowGet);
                                //resp.Content.ReadAsStringAsync().Result;
                                //var resvalues = JObject.Parse(Response);                     
                            }
                            else
                            {
                                return Json(new { status = false, result = "Fail" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else
                    {
                        return Json(new { status = false, result = "User is not activated!" }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { status = false, result = "User not exists!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = false, result = ex.Message }, JsonRequestBehavior.AllowGet);
            }

        }
       
        public ActionResult Role_assigned()
        {
            var loginWith = string.Empty;
            if (UserMgnt.loginType != "")
            {
                loginWith = UserMgnt.loginType;
            }

            //if (loginWith == "facebook")
            //{
            //    string accessCode = Request.QueryString["code"].ToString();

            //    var fb = new FacebookClient();

            //    // throws OAuthException 
            //    dynamic result = fb.Post("oauth/access_token", new
            //    {
            //        client_id = "1205696766486977",
            //        client_secret = "c1e34106bf083041714176327ff502e0",
            //        redirect_uri = redirect_url,
            //        code = accessCode
            //    });

            //    var accessToken = result.access_token;
            //    var expires = result.expires;

            //    // Store the access token in the session
            //    Session["AccessToken"] = accessToken;

            //    // update the facebook client with the access token 
            //    fb.AccessToken = accessToken;

            //    // Calling Graph API for user info
            //    dynamic me = fb.Get("me?fields=id,name,email,gender,photos,location");
            //    //string id = me.id; // You can store it in the database                
            //    //string email = me.email;
            //    string name = me.name;
            //    string[] authorsList = name.Split(' ');

            //    string firstName = authorsList[0].ToString();
            //    string lastName = authorsList[1].ToString();

            //    otp otp = new otp();
            //    UserMgnt u = new UserMgnt();
            //    var genOtp = otp.GenerateOtp();
            //    SignalATMClass _SignalATM = new SignalATMClass();
            //    Hashtable ht_reg = new Hashtable();
            //    ht_reg.Add("@iUserEmail", me.email);
            //    ht_reg.Add("@iUserPassword", "");
            //    ht_reg.Add("@iUserOTP", genOtp);
            //    ht_reg.Add("@iFirstName", firstName);
            //    ht_reg.Add("@iLastName", lastName);
            //    ht_reg.Add("@iSocialMedia", true);
            //    ht_reg.Add("@iSocialMediaID", me.id);
            //    string strResult = _SignalATM.RegisterBySocialMedia(ht_reg);
            //    //if (strResult == "User has been registed.")
            //    //    return Json(new { success = true, result = "OK" }, JsonRequestBehavior.AllowGet);
            //    //else
            //    //    return Json(new { success = true, result = "Failed" }, JsonRequestBehavior.AllowGet);
            //}

             if (loginWith == "gmail")
            {
                String Parameters;
                ///string loginWith = Session["loginWith"].ToString();
                var url = Request.Url.Query;
                String queryString = url.ToString();
                char[] delimiterChars = { '=' };
                string[] words = queryString.Split(delimiterChars);
                string code = words[1];
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                webRequest.Method = "POST";
                Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + redirect_url + "&grant_type=authorization_code";
                byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = byteArray.Length;
                Stream postStream = webRequest.GetRequestStream();
                // Add the post data to the web request
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                WebResponse response = webRequest.GetResponse();
                postStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(postStream);
                string responseFromServer = reader.ReadToEnd();

                GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);

                if (serStatus != null)
                {
                    string accessToken = string.Empty;
                    accessToken = serStatus.access_token;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        // This is where you want to add the code if login is successful.
                        getgoogleplususerdataSerAsync(accessToken);
                    }
                }
            }
            return View();
        }

        private async Task getgoogleplususerdataSerAsync(string accessToken)
        {
            try
            {
                HttpClient client = new HttpClient();
                var urlProfile = "https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accessToken;

                HttpResponseMessage output = await client.GetAsync(urlProfile);

                if (output.IsSuccessStatusCode)
                {
                    string outputData = await output.Content.ReadAsStringAsync();
                    GoogleUserOutputData serStatus = JsonConvert.DeserializeObject<GoogleUserOutputData>(outputData);

                    if (serStatus != null)
                    {
                        // You will get the user information here.
                        userAllInfo.id = serStatus.id;
                        userAllInfo.first_name = serStatus.given_name;
                        userAllInfo.last_name = serStatus.family_name;
                        userAllInfo.Emails = serStatus.email;
                        userAllInfo.locale = serStatus.locale;
                        userAllInfo.picture = serStatus.picture;
                        userAllInfo.name = serStatus.name;
                        Session["UserName"] = serStatus.name;
                        String str = userAllInfo.name;

                        char[] spearator = { ',', ' ' };
                        // using the method 
                        String[] strlist = str.Split(spearator,
                           StringSplitOptions.RemoveEmptyEntries);

                        foreach (String s in strlist)
                        {
                            // string Firstname = s;
                            //string Lastname = s;
                        }
                        otp otp = new otp();
                        UserMgnt u = new UserMgnt();
                        var genOtp = otp.GenerateOtp();
                        SignalATMClass _SignalATM = new SignalATMClass();
                        Hashtable ht_reg = new Hashtable();
                        ht_reg.Add("@iUserEmail", userAllInfo.Emails);
                        ht_reg.Add("@iUserPassword", "");
                        ht_reg.Add("@iUserOTP", genOtp);
                        ht_reg.Add("@iFirstName", userAllInfo.first_name);
                        ht_reg.Add("@iLastName", userAllInfo.last_name);
                        ht_reg.Add("@iSocialMedia", true);
                        ht_reg.Add("@iSocialMediaID", userAllInfo.id);
                        string strResult = _SignalATM.RegisterBySocialMedia(ht_reg);

                        client.CancelPendingRequests();
                    }
                }
                RedirectToAction("Login");
            }
            catch (Exception ex)
            {

            }
        }

        public ActionResult Login_otp()
        {
            return View();
        }
        public ActionResult Reset_Password()
        {
            return View();
        }
        public JsonResult Reset_PasswordDetails(string password)
        {
            if (Session["EmailTo"] != null)
            {
                UserMgnt u = new UserMgnt();
                u.Password = u.Encrypt(password);
                string email = Convert.ToString(Session["EmailTo"].ToString());
                SignalATMClass _SignalATM = new SignalATMClass();
                Hashtable hs = new Hashtable();
                hs.Add("@email", email);
                hs.Add("@password", u.Password);
                string strResult = _SignalATM.UpdatePassword(hs);
                if (strResult == "True")
                {
                    return Json(new { success = true, status = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, status = false }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json(new { success = true, status = "" }, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult Basic_Configuration()
        {           
            List<mstMarket> marketList  = GetMarketList();           
            List<mstInstrument> instList = GetInstrumentList();
            BasicConfiguarationViewModel model = new BasicConfiguarationViewModel();
            model.MarketList = marketList;
            model.InstrumentList = instList;
            ViewBag.InstList = instList;
            return View(model);
        }         
        public static List<mstMarket> GetMarketList()
        {          
            string url = ConfigurationManager.AppSettings["BaseUrl"].ToString() + "serviceproviders/getmarkets/";
           var request = (HttpWebRequest)WebRequest.Create(url);                 
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";            
            var response = (HttpWebResponse)request.GetResponse();
            var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            JObject marketList = new JObject();
            marketList = (JObject)JsonConvert.DeserializeObject(responseString);
            IList<JToken> results = marketList["data"].Children().ToList();
            List<mstMarket> marketResult = new List<mstMarket>();
            foreach (JToken dr in results)
            {
                marketResult.Add(new mstMarket
                {
                    MarketID = Convert.ToInt32(dr["MarketID"]),
                    MarketName = dr["MarketName"].ToString()
                });
            }
            return marketResult;
        }
        public  List<mstInstrument> GetInstrumentList()
        {
            List<mstInstrument> instrumentList = new List<mstInstrument>(); 
            Hashtable inputParam = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DataTable dt = new DataTable();
            dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllInstruments");          
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                 mstInstrument inst = new mstInstrument();
                 inst.InstrumentID = Convert.ToInt32(dt.Rows[i]["InstrumentID"]);
                 inst.InstrumentsName = dt.Rows[i]["InstrumentsName"].ToString();
                inst.MarketId =Convert.ToInt32( dt.Rows[i]["MarketId"]);
                 instrumentList.Add(inst);
            }
            return instrumentList;
        }
        //public static List<mstInstrument> GetInstrumentListByMarketId(int marketId)
        //{
        //         JObject instList = new JObject();
        //        List<mstInstrument> instDataRes = new List<mstInstrument>();

        //        string url = ConfigurationManager.AppSettings["BaseUrl"].ToString() + "GetInstrumentsByMarket?MarketId=";
        //        var request = (HttpWebRequest)WebRequest.Create(url+marketId);
        //        request.Method = "GET";
        //        request.ContentType = "application/x-www-form-urlencoded";
        //        var response = (HttpWebResponse)request.GetResponse();
        //        var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();              
        //        instList = (JObject)JsonConvert.DeserializeObject(responseString);
        //        IList<JToken> results = instList["data"].Children().ToList();
        //        foreach (JToken dr in results)
        //        {
        //            instDataRes.Add(new mstInstrument
        //            {
        //                InstrumentID = Convert.ToInt32(dr["InstrumentID"]),
        //                InstrumentsName = dr["InstrumentsName"].ToString()
        //            });
        //        }
        //        return instDataRes;            
        //}
        public async Task<JsonResult> GetInstrumentListByMarketId(int marketId)
        {
            JObject instList = new JObject();
            List<mstInstrument> instDataRes = new List<mstInstrument>();          
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetInstrumentsByMarket?MarketId=" + marketId);

                if (resp.IsSuccessStatusCode)
                {
                    var instResponse = resp.Content.ReadAsStringAsync().Result;
                    instList = (JObject)JsonConvert.DeserializeObject(instResponse);
                    IList<JToken> results = instList["data"].Children().ToList();
                    foreach (JToken dr in results)
                    {
                        instDataRes.Add(new mstInstrument
                        {
                            InstrumentID = Convert.ToInt32(dr["InstrumentID"]),
                            InstrumentsName = dr["InstrumentsName"].ToString()
                        });
                    }
                    return Json(instDataRes, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        [HttpPost]
        public ActionResult Basic_Configuration(int contrId,int timeZoneId,string instArray)
        {
            try
            {
                sqlDataAccess = new SQLDataAccess();
                _SignalATM = new SignalATMClass();
                string user = Session["UserId"].ToString();
                Hashtable reg_User = new Hashtable();
                reg_User.Add("@strApplicationUserEmailID", user);
                int userId = _SignalATM.GetuserId(reg_User);
                Hashtable inputParam = new Hashtable();
                string xmlInstData = GetXml(instArray, userId);
                inputParam.Add("@iCountryID", contrId);
                inputParam.Add("@iTimeZoneID", timeZoneId);
                inputParam.Add("@iUserID", userId);
              //  inputParam.Add("@iMarketID", marketID);
                inputParam.Add("@xmlData", xmlInstData);
                var response = sqlDataAccess.ExecuteStoreProcedure("usp_Save_BasicConfiguration", inputParam);

                //var request = (HttpWebRequest)WebRequest.Create("http://apps.brainlines.net/SignalATMWebAPI/api/serviceproviders/savebasicconfiguration/");
                //string postData = string.Empty;
                //postData += "&iCountryID=" + contrId + "&iTimeZoneID="+ timeZoneId+ "&iUserID=" + userId+ "&iMarketID=" + marketID+ "&xmlData=" + xmlInstData;             

                //var data = Encoding.ASCII.GetBytes(postData);
                //request.Method = "POST";
                //request.ContentType = "application/x-www-form-urlencoded";
                //request.ContentLength = data.Length;
                //using (var stream = request.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}
                //var response = (HttpWebResponse)request.GetResponse();
                //var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                return View("OK");
            }
            catch (Exception ex)
            {
                return View(ex.Message);
            }
        }
        public string GetXml(IEnumerable list, int userId)
        {
            _SignalATM = new SignalATMClass();
            Hashtable reg_User = new Hashtable();
            reg_User.Add("@UserId", userId);
            int RoleId = _SignalATM.GetuserRoleId(reg_User);
            StringBuilder sbXml = new StringBuilder();
            DateTime entryDate = DateTime.Now.Date;
            sbXml.Append("<InstrumentLists>");
            if (RoleId == 4)
            {
                foreach (object obj in list)
                {
                    sbXml.Append("<InstrumentList>");
                    sbXml.Append("<SigProID>" + userId);
                    // sbXml.Append('"'+ userId + '"');
                    sbXml.Append("</SigProID>");

                    sbXml.Append("<InstrumentID>" + Convert.ToInt32(obj.ToString()));
                    // sbXml.Append('"' + obj.ToString() + '"');                  
                    sbXml.Append("</InstrumentID>");

                    sbXml.Append("<StatusID>");
                    sbXml.Append('"' + 1 + '"');
                    sbXml.Append("</StatusID>");

                    sbXml.Append("<EntryDate>'" + entryDate);
                    // sbXml.Append('"' + DateTime.UtcNow.ToString() + '"');                   
                    sbXml.Append("'</EntryDate>");
                    sbXml.Append("</InstrumentList>");
                }
            }
            else if (RoleId == 2)
            {

                foreach (object obj in list)
                {
                    sbXml.Append("<InstrumentList>");
                    sbXml.Append("<SigProID>" + userId);                                  
                    sbXml.Append("</SigProID>");

                    sbXml.Append("<InstrumentID>" + Convert.ToInt32(obj.ToString()));                                  
                    sbXml.Append("</InstrumentID>");
                    sbXml.Append("</InstrumentList>");
                }
            }
            sbXml.Append("</InstrumentLists>");
            return sbXml.ToString();
        }

        public ActionResult SaveUserRole(string userRole)
        {
             //ViewBag.UserRole = userRole;
            string hostName = Dns.GetHostName();
            string ipAddress = Dns.GetHostByName(hostName).AddressList[0].ToString();
            string userId = Convert.ToString(Session["UserId"]);
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputData = new Hashtable();
            inputData.Add("@strApplicationUserEmailID", userId);
            var strUserID = sqlDataAccess.ExecuteStoreProcedure("usp_Get_UserIdByEmail", inputData);
            int userID = Convert.ToInt32(strUserID);
            if (userRole == "Trader")
            {                      
                 sqlDataAccess = new SQLDataAccess();
                Hashtable hinput = new Hashtable();
                hinput.Add("@strUserReferenceID", strUserID);
                hinput.Add("@strRoleName", userRole);
                hinput.Add("@strUserIPAddresses", ipAddress);
                var queryRes = sqlDataAccess.ExecuteStoreProcedure("usp_sec_UpdateUserRole", hinput);
                
            }
            else if(userRole == "Signal Provider")
            {              
                sqlDataAccess = new SQLDataAccess();
                Hashtable hinput = new Hashtable();
                hinput.Add("@strUserReferenceID", strUserID);
                hinput.Add("@strRoleName", userRole);
                hinput.Add("@strUserIPAddresses", ipAddress);
                var queryRes = sqlDataAccess.ExecuteStoreProcedure("usp_sec_UpdateUserRole", hinput);
            }
            else if(userRole == "Referral")
            {               
                sqlDataAccess = new SQLDataAccess();
                Hashtable hinput = new Hashtable();
                hinput.Add("@strUserReferenceID", strUserID);
                hinput.Add("@strRoleName", userRole);
                hinput.Add("@strUserIPAddresses", ipAddress);
                var queryRes = sqlDataAccess.ExecuteStoreProcedure("usp_sec_UpdateUserRole", hinput);
            }
            else
            {

            }
            return View();
        }

        public JsonResult GetAllCountry()
        {
            List<mstCountry> countryList = new List<mstCountry>();
            sqlDataAccess = new SQLDataAccess();
            var  result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");
                       
            foreach (DataRow dr in result.Rows)
            {
                countryList.Add(new mstCountry
                {
                    CountryID = Convert.ToInt32(dr["CountryID"]),
                    Country = dr["Country"].ToString()
                });
           }       

            return Json(countryList, JsonRequestBehavior.AllowGet);
        }    
        public JsonResult GetTimeZone()
        {
            List<Models.TimeZone> timeZoneList = new List<Models.TimeZone>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTimeZones");

            foreach (DataRow dr in result.Rows)
            {
                timeZoneList.Add(new Models.TimeZone
                {
                    TimeZoneID = Convert.ToInt32(dr["TimeZoneID"]),
                    TimeZoneDetails = dr["TimeZoneDetails"].ToString()
                });
            }
            return Json(timeZoneList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Enter_otp()
        {
            return View();
        }

        public ActionResult Google()
        {
            UserMgnt.loginType = "gmail";
            var Googleurl = "https://accounts.google.com/o/oauth2/auth?response_type=code&redirect_uri=" + redirect_url + "&scope=https://www.googleapis.com/auth/userinfo.email%20https://www.googleapis.com/auth/userinfo.profile&client_id=" + googleplus_client_id;
            Response.Redirect(Googleurl); //831162440368 - gjpqkqbdj5mik8adij6s313qt51kqjor.apps.googleusercontent.com
            ViewBag.Message = "Your contact page.";
            return View();

        }
        //public ActionResult FaceBookLogin()
        //{
        //    UserMgnt.loginType = "facebook";
        //    loginType = "facebook";
        //    var fb = new FacebookClient();
        //    var loginUrl = fb.GetLoginUrl(new
        //    {
        //        client_id = "581533035929980",
        //        redirect_uri = redirect_url,
        //        scope = "email" /// Add other permissions as needed

        //    });
        //    Response.Redirect(loginUrl.AbsoluteUri);
        //    return View();
        //}
        //public ActionResult LinkedinLogin()        //{        //    return View();        //}        //public ActionResult LinkedinLogin(string code, string state)        //{
        //    LinkedInConnect.APIKey = "81oxu10ane59nm";
        //    LinkedInConnect.APISecret = "t8Js3GQOsGqavseM";
        //    LinkedInConnect.RedirectUrl = string.Format("{0}://{1}/{2}", Request.Url.Scheme, Request.Url.Authority, "https://localhost:44351/Home/Role_assigned");        //    return View();        //}
        [HttpPost]
        public EmptyResult Login1()
        {
            //if (!LinkedInConnect.IsAuthorized)
            //{
            //    LinkedInConnect.Authorize();
            //}

            return new EmptyResult();
        }

        //public ActionResult LinkedinLogin()
        //{
        //    return View();
        //}

        public ActionResult tr_documents()
        {
            return View();
        }
        public ActionResult tr_faq()
        {
            return View();
        }
        public ActionResult tr_global_profile()
        {
            return View();
        }
        public ActionResult tr_myportal_signals()
        {
            return View();
        }
       
        public ActionResult tr_my_signals()
        {
            var loginWith = string.Empty;
            if (UserMgnt.loginType != "")
            {
                loginWith = UserMgnt.loginType;
            }

            //if (loginWith == "facebook")
            //{
            //    string ac = Request.QueryString.ToString();
            //    string accessCode = Request.QueryString["code"].ToString();

            //    var fb = new FacebookClient();

            //    // throws OAuthException 
            //    dynamic result = fb.Post("oauth/access_token", new
            //    {
            //        // client_id = "1205696766486977",
            //        // client_secret = "c1e34106bf083041714176327ff502e0",//Sandeep 
            //           client_id = "581533035929980",
            //           client_secret = "237a909bb5c6b75fc6ec52b763f26515",
            //        redirect_uri = redirect_url,
            //        code = accessCode
            //    });

            //    var accessToken = result.access_token;
            //    var expires = result.expires;

            //    // Store the access token in the session
            //    Session["AccessToken"] = accessToken;

            //    // update the facebook client with the access token 
            //    fb.AccessToken = accessToken;

            //    // Calling Graph API for user info
            //    dynamic me = fb.Get("me?fields=id,name,email,gender,photos,location");
            //    //string id = me.id; // You can store it in the database                
            //    //string email = me.email;
            //    string name = me.name;
            //    string[] authorsList = name.Split(' ');

            //    string firstName = authorsList[0].ToString();
            //    string lastName = authorsList[1].ToString();

            //    otp otp = new otp();
            //    UserMgnt u = new UserMgnt();
            //    var genOtp = otp.GenerateOtp();
            //    SignalATMClass _SignalATM = new SignalATMClass();
            //    Hashtable ht_reg = new Hashtable();
            //    ht_reg.Add("@iUserEmail", me.email);
            //    ht_reg.Add("@iUserPassword", "");
            //    ht_reg.Add("@iUserOTP", genOtp);
            //    ht_reg.Add("@iFirstName", firstName);
            //    ht_reg.Add("@iLastName", lastName);
            //    ht_reg.Add("@iSocialMedia", true);
            //    ht_reg.Add("@iSocialMediaID", me.id);
            //    string strResult = _SignalATM.RegisterBySocialMedia(ht_reg);
            //    //if (strResult == "User has been registed.")
            //    //    return Json(new { success = true, result = "OK" }, JsonRequestBehavior.AllowGet);
            //    //else
            //    //    return Json(new { success = true, result = "Failed" }, JsonRequestBehavior.AllowGet);
            //}
            if (loginWith =="gmail")
            {
                String Parameters;
                ///string loginWith = Session["loginWith"].ToString();
                var url = Request.Url.Query;
                String queryString = url.ToString();
                char[] delimiterChars = { '=' };
                string[] words = queryString.Split(delimiterChars);
                string code = words[1];
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create("https://accounts.google.com/o/oauth2/token");
                webRequest.Method = "POST";
                Parameters = "code=" + code + "&client_id=" + googleplus_client_id + "&client_secret=" + googleplus_client_secret + "&redirect_uri=" + redirect_url + "&grant_type=authorization_code";
                byte[] byteArray = Encoding.UTF8.GetBytes(Parameters);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.ContentLength = byteArray.Length;
                Stream postStream = webRequest.GetRequestStream();
                // Add the post data to the web request
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();

                WebResponse response = webRequest.GetResponse();
                postStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(postStream);
                string responseFromServer = reader.ReadToEnd();

                GooglePlusAccessToken serStatus = JsonConvert.DeserializeObject<GooglePlusAccessToken>(responseFromServer);
                if (serStatus != null)
                {
                    string accessToken = string.Empty;
                    accessToken = serStatus.access_token;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        // This is where you want to add the code if login is successful.
                        getgoogleplususerdataSerAsync(accessToken);
                    }
                }
            }
            return View();
        }
        public ActionResult tr_plan_payments()
        {
            return View();
        }
        public ActionResult tr_profile()
        {
            return View();
        }
        public ActionResult tr_signal_preferences()
        {

            return View();
        }
        public ActionResult tr_subscription_checkout()
        {

            return View();
        }
        public ActionResult tr_subscription_payment()
        {
            return View();
        }
        public ActionResult tr_subscription_plan()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult InvalidUser()
        {
            return View();
        }
        public ActionResult SendPasswordLink()
        {
            return View();
        }
        public ActionResult SavePasswordMail()
        {
            return View();
        }
       
    }
}