﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SignalATM.Web.Models;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Collections;
using System.Text;
using System.Data;
using System.Configuration;
using InfoFreightSystem;
using System.IO;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using System.Net;
using Razorpay.Api;

namespace SignalATM.Web.Controllers
{
    public class SignalProviderController : Controller
    {
        // GET: SignalProvider
        InfoFreightSystem.SQLDataAccess sqlDataAccess;
        Hashtable inputParams;
        public GlobalVariables globalVaribles;
        public static string orderId;
        public SignalProviderController()
        {
            try
            {
               // string emailId = "karapesandeep@gmail.com";
                string emailId = System.Web.HttpContext.Current.Session["UserId"].ToString();                             
                sqlDataAccess = new SQLDataAccess();
                inputParams = new Hashtable();
                inputParams.Add("@strApplicationUserLoginName", emailId);
                globalVaribles = new GlobalVariables();
                DataTable dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetGlobalVariablesData", inputParams);
                globalVaribles.SecApplicationUserID = Convert.ToInt32(dt.Rows[0]["ApplicationUserID"]);
                globalVaribles.SecUserID = Convert.ToInt32(dt.Rows[0]["UserID"]);
                globalVaribles.RoleID = Convert.ToInt32(dt.Rows[0]["RoleID"]);
                globalVaribles.RoleName = dt.Rows[0]["RoleName"].ToString();
                globalVaribles.UserType = dt.Rows[0]["UserTypeID"].ToString();
                globalVaribles.ApplicationUserPhoneNo = dt.Rows[0]["ApplicationUserPhoneNumber"].ToString();
                globalVaribles.ApplicationUserEmailID = dt.Rows[0]["ApplicationUserEmailID"].ToString();
                double walletAmt = Convert.ToDouble(dt.Rows[0]["WalletBalance"]);
                globalVaribles.UserWalletBalance = (float)walletAmt;
                globalVaribles.PlanID = Convert.ToInt32(dt.Rows[0]["PlanID"]);
                globalVaribles.PeriodID = Convert.ToInt32(dt.Rows[0]["PeriodID"]);
                globalVaribles.MarketID = Convert.ToInt32(dt.Rows[0]["MarketID"]);
                globalVaribles.CountryID = Convert.ToInt32(dt.Rows[0]["CountryID"]);
                globalVaribles.TimezoneID = Convert.ToInt32(dt.Rows[0]["TimeZoneID"]);
                globalVaribles.PlanExpiryDate = Convert.ToDateTime(dt.Rows[0]["ValidTill"]);

                ViewData["WalletAmount"] = globalVaribles.UserWalletBalance;
            }
            catch (Exception ex)
            {

            }
        }
        public JsonResult Getmarkets()
        {
            List<mstMarket> MarketList = new List<mstMarket>();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetMarketsForGlobalVariables",inputParams);
            foreach (DataRow dr in result.Rows)
            {
                MarketList.Add(new mstMarket
                {
                    MarketID = Convert.ToInt32(dr["MarketID"]),
                    MarketName = dr["MarketName"].ToString()
                });
            }

            return Json(MarketList, JsonRequestBehavior.AllowGet);
        }
        public void SetGlobalMarket(int marketId)
        {
            globalVaribles.MarketID = marketId;
        }

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SP_CreateNew()
        {
            return View();
        }
        public ActionResult Empanelmentprocess()
        {
            return View();
        }
        #region Profile 
        public ActionResult PersonalDetails()
        {
            ViewBag.UserEmail = Session["UserId"].ToString();
            ViewBag.userID = globalVaribles.SecUserID;
            return View();
        }
        public string GetPersonalDetails()
        {
            inputParams = new Hashtable();
            inputParams.Add("@UserID", globalVaribles.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPersonalDetails", inputParams);
            string pic = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserImage"]);
            ViewBag.UserImage = pic;
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult GetPersonalDetailsForGlobalProfile()
        {
            string str = "";
            try
            {

                inputParams = new Hashtable();
                inputParams.Add("@UserID", globalVaribles.SecUserID);
                sqlDataAccess = new SQLDataAccess();
                String[] DocArray = new String[6];
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPersonalDetails", inputParams);

                if (dt.Rows.Count == 0)
                {
                    str = "0";
                }
                else
                {
                    DocArray[0] = dt.Rows[0]["UserName"].ToString();
                    DocArray[1] = dt.Rows[0]["AboutMe"].ToString();
                    DocArray[2] = dt.Rows[0]["Email"].ToString();
                    DocArray[3] = dt.Rows[0]["EntryDate"].ToString();
                    DocArray[4] = dt.Rows[0]["ApplicationUserPhoneNumber"].ToString();
                    DocArray[5] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserImage"]);
                }

                return Json(new { success = true, response = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { success = true, response = str }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SubmitPersonalDetails(string result)
        {
            try
            {
                var fileName = "";
                var path = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                    file.SaveAs(path);
                }
                byte[] theBytes = Encoding.UTF8.GetBytes(fileName);

                dynamic obj = JsonConvert.DeserializeObject(result);
                inputParams = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                inputParams.Add("@iUserID", globalVaribles.SecUserID);
                inputParams.Add("@strUserName", obj.Name.ToString());
                inputParams.Add("@strDisplayName", obj.DisplayName.ToString());
                inputParams.Add("@strEmail", obj.Email.ToString());
                inputParams.Add("@strUserPhoneNO", obj.Phone.ToString());
                inputParams.Add("@iGender", obj.Gender.ToString());
                inputParams.Add("@strUserAddress", obj.Address.ToString());
                inputParams.Add("@iCountryID", obj.Country.ToString());
                inputParams.Add("@iTimeZone", obj.TimeZone.ToString());
                inputParams.Add("@strState", obj.State.ToString());
                inputParams.Add("@strCity", obj.City.ToString());
                inputParams.Add("@strAboutMe", obj.AboutMe.ToString());
                inputParams.Add("@iUserReferenceID", globalVaribles.SecApplicationUserID);
                inputParams.Add("@strUserUrl", obj.Website.ToString());
                inputParams.Add("@bUserImage", theBytes);
                string resp = sqlDataAccess.ExecuteStoreProcedure("usp_SavePersonalDetails", inputParams);
                return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return Json(new { success = true, response = msg }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult EditPersonalDetails(string result)
        {
            try
            {
                var fileName = "";
                var path = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    fileName = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("../Content/CSS/img"), fileName);
                    file.SaveAs(path);
                }
                byte[] theBytes = Encoding.UTF8.GetBytes(fileName);
                dynamic obj = JsonConvert.DeserializeObject(result);
                inputParams = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                inputParams.Add("@iUserID", globalVaribles.SecUserID);
                inputParams.Add("@strUserName", obj.Name.ToString());
                inputParams.Add("@strDisplayName", obj.DisplayName.ToString());
                inputParams.Add("@strEmail", obj.Email.ToString());
                inputParams.Add("@strUserPhoneNO", obj.Phone.ToString());
                inputParams.Add("@iGender", obj.Gender.ToString());
                inputParams.Add("@strUserAddress", obj.Address.ToString());
                inputParams.Add("@iCountryID", obj.Country.ToString());
                inputParams.Add("@iTimeZone", obj.TimeZone.ToString());
                inputParams.Add("@strState", obj.State.ToString());
                inputParams.Add("@strCity", obj.City.ToString());
                inputParams.Add("@strAboutMe", obj.AboutMe.ToString());
                inputParams.Add("@iUserReferenceID", globalVaribles.SecApplicationUserID);
                inputParams.Add("@strUserUrl", obj.Website.ToString());
                inputParams.Add("@bUserImage", theBytes);
                string resp = sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePersonalDetails", inputParams);
                return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return Json(new { success = true, response = msg }, JsonRequestBehavior.AllowGet);
            }

        }
        public string GetContryCityStateGender()
        {
            inputParams = new Hashtable();
            inputParams.Add("@", globalVaribles.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");
            var dt1 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllStateByCountryID", inputParams);
            var dt2 = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllCityByStateID", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public ActionResult GetCountries()
        {
            List<mstCountry> countryList = new List<mstCountry>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetCountry");
            foreach (DataRow r in dt.Rows)
            {
                countryList.Add(new mstCountry
                {
                    CountryID = Convert.ToInt32(r["CountryID"]),
                    Country = r["Country"].ToString()
                });
            }
            return Json(new { success = true, CountryList = countryList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetStates(int countryID)
        {
            List<mstState> stateList = new List<mstState>();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iCountryID", countryID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllStateByCountryID", inputParams);
            foreach (DataRow r in dt.Rows)
            {
                stateList.Add(new mstState
                {
                    StateID = Convert.ToInt32(r["StateID"]),
                    StateName = r["StateName"].ToString()
                });
            }
            return Json(new { success = true, StateList = stateList }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCities(int stateID)
        {
            List<mstCity> cityList = new List<mstCity>();
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iStateID", stateID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetAllCityByStateID", inputParams);
            foreach (DataRow r in dt.Rows)
            {
                cityList.Add(new mstCity
                {
                    CityID = Convert.ToInt32(r["CityID"]),
                    CityName = r["CityName"].ToString()
                });
            }
            return Json(new { success = true, CityList = cityList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetGenders()
        {
            List<mstGender> GenderList = new List<mstGender>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetGender");
            foreach (DataRow r in dt.Rows)
            {
                GenderList.Add(new mstGender
                {
                    GenderID = Convert.ToInt32(r["GenderID"]),
                    Gender = r["Gender"].ToString()
                });
            }
            return Json(new { success = true, Genders = GenderList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetTimeZones()
        {
            List<Models.TimeZone> timezoneList = new List<Models.TimeZone>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTimeZones");
            foreach (DataRow r in dt.Rows)
            {
                timezoneList.Add(new Models.TimeZone
                {
                    TimeZoneID = Convert.ToInt32(r["TimeZoneID"]),
                    TimeZoneDetails = r["TimeZoneDetails"].ToString()
                });
            }
            return Json(new { success = true, TimeZoneslist = timezoneList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangePassword(string email, string password)
        {
            UserMgnt u = new UserMgnt();
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string pwd = u.Encrypt(password);
            inputParams.Add("@email", email);
            inputParams.Add("@password", pwd);
            string resp = sqlDataAccess.ExecuteStoreProcedure("usp_UpdatePassword", inputParams);
            return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GlobalProfile()
        {
            List<SelectListItem> items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            DataTable periodDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPeriod");
            if (periodDT.Rows.Count > 0 && periodDT != null)
            {
                foreach (DataRow dr in periodDT.Rows)
                {
                    items.Add(new SelectListItem
                    {
                        Text = "Period " + dr["SignalPerformanceEarningsPeriodID"].ToString(),
                        Value = dr["SignalPerformanceEarningsPeriodID"].ToString()
                    });
                }
            }
            ViewBag.Period = items;
            return View();

        }
        public ActionResult Documents()
        {
            return View();
        }
        public ActionResult GetUserDocument()
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iUserID", globalVaribles.SecUserID);
                sqlDataAccess = new SQLDataAccess();
                String[] DocArray = new String[7];
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserDocumentDetails", inputParams);
                DocArray[0] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserPhoto"]);
                DocArray[1] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserOnePageProfile"]);
                DocArray[2] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserIdentity"]);
                DocArray[3] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserAddressProof"]);
                DocArray[4] = dt.Rows[0]["UserBankDetails"].ToString();
                DocArray[5] = Encoding.ASCII.GetString((byte[])dt.Rows[0]["UserBankChequeStatement"]);
                DocArray[6] = dt.Rows[0]["AdminComments"].ToString();
                // string str = JsonConvert.SerializeObject(DocArray);
                return Json(new { success = true, response = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, response = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult UploadUserDocument(string bankdetails)
        {
            try
            {
                int flagPhoto = 0;
                int flagUserProfile = 0;
                int flagIdProf = 0;
                int flagAddProf = 0;
                int flagBankStatement = 0;
                var defaultVal = new byte[16];
                byte[] userPhoto = defaultVal;
                byte[] profile = defaultVal;
                byte[] idProf = defaultVal;
                byte[] addressProf = defaultVal;
                byte[] bankStatement = defaultVal;

                String[] files = new String[5];
                var path = "";
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];
                    files[i] = Path.GetFileName(file.FileName);
                    path = Path.Combine(Server.MapPath("~/Content/CSS/img"), files[i]);
                    file.SaveAs(path);
                    if (Request.Files.AllKeys[i] == "userPhoto")
                    {
                        flagPhoto = 1;
                        userPhoto = Encoding.UTF8.GetBytes(files[i]);
                    }
                    else if (Request.Files.AllKeys[i] == "profile")
                    {
                        profile = Encoding.UTF8.GetBytes(files[i]);
                        flagUserProfile = 1;
                    }
                    else if (Request.Files.AllKeys[i] == "idProf")
                    {
                        idProf = Encoding.UTF8.GetBytes(files[i]);
                        flagIdProf = 1;
                    }
                    else if (Request.Files.AllKeys[i] == "addProf")
                    {
                        addressProf = Encoding.UTF8.GetBytes(files[i]);
                        flagAddProf = 1;
                    }
                    else if (Request.Files.AllKeys[i] == "bankStatement")
                    {
                        bankStatement = Encoding.UTF8.GetBytes(files[i]);
                        flagBankStatement = 1;
                    }
                }

                inputParams = new Hashtable();
                sqlDataAccess = new SQLDataAccess();
                inputParams.Add("@iUserID", globalVaribles.SecUserID);
                inputParams.Add("@iUserRole", globalVaribles.RoleID);
                inputParams.Add("@imgUserPhoto", userPhoto);
                inputParams.Add("@imgUserProfile", profile);
                inputParams.Add("@imgUserIdentity", idProf);
                inputParams.Add("@imgUserAddressProof", addressProf);
                inputParams.Add("@strUserBankDetails", bankdetails);
                inputParams.Add("@imgUserChequeStatement", bankStatement);
                inputParams.Add("@bIsApproved", 1);
                inputParams.Add("@iFlagUserPhoto", flagPhoto);
                inputParams.Add("@iFlagUserProfile", flagUserProfile);
                inputParams.Add("@iFlagUserIdentity", flagIdProf);
                inputParams.Add("@iFlagUserAddressProof", flagAddProf);
                inputParams.Add("@iFlagUserChequeStatement", flagBankStatement);
                string resp = sqlDataAccess.ExecuteStoreProcedure("usp_UploadUserDocuments", inputParams);
                return Json(new { success = true, response = resp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                string msg = e.Message;
                return Json(new { success = true, response = msg }, JsonRequestBehavior.AllowGet);
            }

        }
        #endregion
        #region My Plan 

        public ActionResult Empanelment()
        {
            return View();
        }
        public ActionResult GetEmpanelmentData()
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@iSignalProID", globalVaribles.SecUserID);
            var response = sqlDataAccess.GetDatasetExecuteStoredProcedure("usp_GetEmpanelmentDetails", inputParams);
            var myarray = new
            {
                Criteria = JsonConvert.SerializeObject(response.Tables[0]),
                Performance = JsonConvert.SerializeObject(response.Tables[1]),
                History = JsonConvert.SerializeObject(response.Tables[2]),
            };
            return Json(myarray, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Subscription()
        {
            ViewBag.RoleID = globalVaribles.RoleID;
            DataSet userSubScriptionDT = new DataSet();
            string userId = System.Web.HttpContext.Current.Session["UserId"].ToString();
            if (userId != "")
            {
                // ViewBag.RoleID = globalVaribles.RoleID;
                sqlDataAccess = new SQLDataAccess();
                inputParams = new Hashtable();
                inputParams.Add("@iUserID", globalVaribles.SecUserID);
                userSubScriptionDT = sqlDataAccess.GetDatasetExecuteStoredProcedure("usp_Get_SubcriptionPlan", inputParams);

            }
            return View(userSubScriptionDT);
        }
        public ActionResult GetSubscriptionPlanDetails(int roleId)
        {
            // var planList = from a in UserPlan where (a.RoleID = roleId) && (a.StatusID = 1) select a;
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            List<UserPlan> premiumUsers = new List<UserPlan>();
            List<UserPlan> standerdUsers = new List<UserPlan>();
            string PlanType = string.Empty;
            inputParams.Add("@iRoleId", roleId);
            var dataTable = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserPlan", inputParams);
            string str = JsonConvert.SerializeObject(dataTable);
            return Json(str, JsonRequestBehavior.AllowGet);
        }
        public string SaveSubscriptionPlanDetails(string result)
        {
            dynamic obj = JsonConvert.DeserializeObject(result);
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            inputParams.Add("@iPlanID", obj.UserPlansID.ToString());
            inputParams.Add("@siValidity", obj.Validity.ToString());
            inputParams.Add("@iStatusID", 1);
            inputParams.Add("@iPaymentStatusID", 1);
            inputParams.Add("@sdtFromDate", obj.FromDate.ToString());
            var response = sqlDataAccess.ExecuteStoreProcedure("usp_Save_UserPlanSubscriptions", inputParams);
            return response;
        }
        public ActionResult SubscriptionCheckOut(int planId)
        {
            ViewBag.UserId = globalVaribles.SecUserID;
            ViewBag.UserPhoneNo = globalVaribles.ApplicationUserPhoneNo;
            ViewBag.Email = globalVaribles.ApplicationUserEmailID;
            // ViewBag.UserName = globalVaribles.UserName;
            ViewBag.WalletBalance = globalVaribles.UserWalletBalance;
            ViewBag.PlanID = planId;
            return View();
        }
        public ActionResult NextToPaymentAction(string result)
        {
            Session["paymentInfo"] = result;
            string key = "rzp_test_6SmO037aAU6umV";
            string secret = "oSCggMVxeJvmbSWrjZ26MECq";
            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
            RazorpayClient client = new RazorpayClient(key, secret);
            dynamic obj = JsonConvert.DeserializeObject(result);
            Dictionary<string, object> options = new Dictionary<string, object>();
            string amtStr = Convert.ToString(obj.NetPaymentSourceAmt);
            string[] arrayNetAmt = amtStr.Split('.');
            Int32 netAmt = Convert.ToInt32(arrayNetAmt[0] + arrayNetAmt[1]);
            options.Add("amount", netAmt); // amount in the smallest currency unit
            options.Add("receipt", "order_rcptid_11");
            options.Add("currency", "INR");
            options.Add("payment_capture", "0");
            Order order = client.Order.Create(options);
            orderId = order["id"].ToString();
            ViewData["OrderID"] = orderId;
            ViewData["netAmt"] = netAmt;
            return View();
        }
        public string GetSubscribedUserDetails(int planId)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            inputParams.Add("@iPlanID", planId);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSubscribedPlanDetails", inputParams);
            string result = JsonConvert.SerializeObject(dt);
            return result;
        }
        public string GetPromocoupons()
        {
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPromoCoupons");
            string result = JsonConvert.SerializeObject(dt);
            return result;
        }
        public ActionResult SaveSubscriberPayment(string result)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            dynamic obj = JsonConvert.DeserializeObject(result);
            string tranzactionDetails = "Tranzaction Successfull";
            inputParams.Add("@iBillID", obj.BillID.ToString());
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            inputParams.Add("@WalletAmountUsed", obj.WalletAmt.ToString());
            inputParams.Add("@dTotalAmount", obj.TotamAmt.ToString());
            inputParams.Add("@strCouponCode", obj.CouponCode.ToString());
            inputParams.Add("@dLessDiscountAmount", obj.LessDiscountAmt.ToString());
            inputParams.Add("@iPaymentSourceID", 2);
            inputParams.Add("@NetPaymentSourceAmount", obj.NetPaymentSourceAmt.ToString());
            inputParams.Add("@iPaymentTypeID", 1);
            inputParams.Add("@strTransactionDetails", tranzactionDetails);
            inputParams.Add("@dGST", 0);
            inputParams.Add("@iPaymentStatusID", 1);
            var responce = sqlDataAccess.ExecuteStoreProcedure("usp_Save_UserPlanSubscriptionPayments", inputParams);
            return Json(responce, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SuccessPayment()
        {
            string paymentId = Request.Form["razorpay_payment_id"];
            string razorpayOrderId = Request.Form["razorpay_order_id"];
            string razorpaySignature = Request.Form["razorpay_signature"];
            string key = "rzp_test_6SmO037aAU6umV";
            string secret = "oSCggMVxeJvmbSWrjZ26MECq";
            var paymentInfo = (string)Session["paymentInfo"];
            dynamic obj = JsonConvert.DeserializeObject(paymentInfo);

            Dictionary<string, object> input = new Dictionary<string, object>();
            input.Add("amount", obj.NetPaymentSourceAmt); // this amount should be same as transaction amount

            RazorpayClient client = new RazorpayClient(key, secret);

            Dictionary<string, string> attributes = new Dictionary<string, string>();

            attributes.Add("razorpay_payment_id", paymentId);
            attributes.Add("razorpay_order_id", Request.Form["razorpay_order_id"]);
            attributes.Add("razorpay_signature", Request.Form["razorpay_signature"]);

            Utils.verifyPaymentSignature(attributes);

            RazorpayClient client3 = new RazorpayClient(key, secret);
            Payment payment = client3.Payment.Fetch(paymentId);

            Dictionary<string, object> options = new Dictionary<string, object>();
            string amtStr = obj.NetPaymentSourceAmt.ToString();
            string[] amtArray = amtStr.Split('.');
            Int32 finalAmt = Convert.ToInt32(amtArray[0] + amtArray[1]);
            options.Add("amount", finalAmt);
            options.Add("currency", "INR");
            Payment paymentCaptured = payment.Capture(options);

            //dynamic obj1 = JsonConvert.DeserializeObject(attr);

            RazorpayClient client2 = new RazorpayClient(key, secret);
            Payment payment2 = client2.Payment.Fetch(paymentId);
            if (payment2 != null)
            {
                Int32 razorAmountRefunded = 0, razorWallet = 0, razorFee = 0, razorTax = 0;
                string razorPaymentId = payment2.Attributes["id"].ToString();
                Int32 razorPaymentAmt = payment2.Attributes["amount"];
                string razorPaymentCur = payment2.Attributes["currency"].ToString();
                string razorOrder_id = payment2.Attributes["order_id"].ToString();
                string razorMethod = payment2.Attributes["method"].ToString();
                razorAmountRefunded = payment2.Attributes["amount_refunded"];
                string razorRefundStatus = payment2.Attributes["refund_status"].ToString();
                string razorCardId = payment2.Attributes["card_id"].ToString();
                string razorBank = payment2.Attributes["bank"].ToString();
                if (payment2.Attributes["wallet"] != null)
                {
                    razorWallet = Convert.ToInt32(payment2.Attributes["wallet"]);
                }
                string razorVpa = payment2.Attributes["vpa"].ToString();
                string razorEmail = payment2.Attributes["email"].ToString();
                string razorContact = payment2.Attributes["contact"].ToString();
                if (payment2.Attributes["fee"] != null)
                {
                    razorFee = payment2.Attributes["fee"];
                }
                if (payment2.Attributes["tax"] != null)
                {
                    razorTax = payment2.Attributes["tax"];
                }
                string razorErrorCode = payment2.Attributes["error_code"].ToString();
                string razorErrorDescription = payment2.Attributes["error_description"].ToString();
                string razorCreatedAt = payment2.Attributes["created_at"].ToString();

                sqlDataAccess = new SQLDataAccess();
                inputParams = new Hashtable();
                // dynamic obj = JsonConvert.DeserializeObject(paymentInfo);
                string tranzactionDetails = "Transaction Successfull";
                inputParams.Add("@iBillID", Convert.ToInt32(obj.BillID.ToString()));
                inputParams.Add("@iUserID", Convert.ToInt32(globalVaribles.SecUserID));
                inputParams.Add("@WalletAmountUsed", Convert.ToDecimal(obj.WalletAmt.ToString()));
                inputParams.Add("@dTotalAmount", Convert.ToDecimal(obj.TotamAmt.ToString()));
                inputParams.Add("@strCouponCode", obj.CouponCode.ToString());
                inputParams.Add("@dLessDiscountAmount", Convert.ToDecimal(obj.LessDiscountAmt.ToString()));
                inputParams.Add("@iPaymentSourceID", 2);
                inputParams.Add("@NetPaymentSourceAmount", Convert.ToDecimal(obj.NetPaymentSourceAmt.ToString()));
                inputParams.Add("@iPaymentTypeID", 1);
                inputParams.Add("@strTransactionDetails", tranzactionDetails);
                inputParams.Add("@dGST", 0);
                inputParams.Add("@iPaymentStatusID", 1);
                inputParams.Add("@sRazorpaymentId", paymentId);
                inputParams.Add("@sRazorOrderID", razorpayOrderId);
                inputParams.Add("@sRazorSignatureID", razorpaySignature);
                inputParams.Add("@iAmount", razorPaymentAmt);
                inputParams.Add("@sCurrency", razorPaymentCur);
                inputParams.Add("@sMethod", razorMethod);
                inputParams.Add("@bRefundStatus", razorRefundStatus);
                inputParams.Add("@iAmountRefunded", razorAmountRefunded);
                inputParams.Add("@sCardId", razorCardId);
                inputParams.Add("@sBank", razorBank);
                inputParams.Add("@iWallet", razorWallet);
                inputParams.Add("@svpa", razorVpa);
                inputParams.Add("@sRazorEmail", razorEmail);
                inputParams.Add("@sRazorContact", razorContact);
                inputParams.Add("@iRazorFee", razorFee);
                inputParams.Add("@iRazorTax", razorTax);
                inputParams.Add("@sRazorErrorCode", razorErrorCode);
                inputParams.Add("@sRazorErrorDescription", razorErrorDescription);
                inputParams.Add("@sRazorCreatedAt", razorCreatedAt);
                var responce = sqlDataAccess.ExecuteStoreProcedure("usp_Save_UserPlanSubscriptionPayments", inputParams);
            }
            //  return PlanPayments();
            return RedirectToAction("PlanPayments", "SignalProvider");
        }
        #endregion

        #region My Signals
        public ActionResult CreateNew()
        {
            return View();
        }
        public ActionResult UpdateSignal(string result)
        {
            try
            {
                string userName = Session["UserId"].ToString();
                dynamic obj = JsonConvert.DeserializeObject(result);
                sqlDataAccess = new SQLDataAccess();
                inputParams = new Hashtable();
                inputParams.Add("@iSignalsID", Convert.ToInt32(obj.SignalId));
                inputParams.Add("@iStlD", Convert.ToInt32(obj.SignalType));
                inputParams.Add("@iSigProID", globalVaribles.SecUserID);
                inputParams.Add("@iBuySellID", Convert.ToInt32(obj.BuySell));
                inputParams.Add("@dStrikePrice", Convert.ToDecimal(obj.StrikePrice));
                inputParams.Add("@dTargetPrice", Convert.ToDecimal(obj.TakeProfit));
                inputParams.Add("@dStopLoss", Convert.ToDecimal(obj.StopLoss));
                inputParams.Add("@dInstPrice", Convert.ToDecimal(obj.LivePrice));
                inputParams.Add("@strDirectionCheck", string.Empty);
                inputParams.Add("@strComments", Convert.ToString(obj.Comment));
                inputParams.Add("@iMINTimeFrameID", Convert.ToInt32(obj.TimeMinFrame));
                inputParams.Add("@iMaxTimeFrameID", Convert.ToInt32(obj.timeMaxFrame));
                inputParams.Add("@iMinValidityID", Convert.ToInt32(obj.ValidityMin));
                inputParams.Add("@iMaxValidityID", Convert.ToInt32(obj.ValidityMax));
                inputParams.Add("@iReliabilityID", Convert.ToInt32(obj.Reliability));
                inputParams.Add("@iRiskID", Convert.ToInt32(obj.RiskType));
                inputParams.Add("@iProcessStatusID", Convert.ToInt32(obj.Status));
                inputParams.Add("@iSuccessStatusID", 1);
                inputParams.Add("@dClosingPips", 0.0);
                inputParams.Add("@dClosingPercent", 0);
                inputParams.Add("@sdtClosingDate", DateTime.Now);
                inputParams.Add("@strUpdatedBy", userName);
                inputParams.Add("@dLivePrice", Convert.ToDecimal(obj.LivePrice));
                var responce = sqlDataAccess.ExecuteStoreProcedure("usp_UpdateSignals", inputParams);

                return Json(new { success = true, result = responce }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Workspace()
        {
            // Session["UserImage"] = globalVaribles.UserImage;
            Hashtable inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            string userName = Session["UserId"].ToString();
         //   string userName = "karapesandeep@gmail.com";//Session["UserId"].ToString(); temparary comment
           // Session["UserId"] = "karapesandeep@gmail.com";//temp
            inputParams.Add("@strApplicationUserLoginName", userName);
            var userId = sqlDataAccess.ExecuteStoreProcedure("usp_GetApplicationUserID", inputParams);
            ViewBag.UserID = userId;
            //  DisplayLiveData dls = new DisplayLiveData();             
            return View();
        }
        //public string GetLiveDataFeed(string instrumentName)
        //{          
        //    inputParams = new Hashtable();
        //    sqlDataAccess = new SQLDataAccess();
        //    inputParams.Add("@strInstrumentName", instrumentName);
        //    var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetLiveInstrumentData", inputParams);
        //    string str = JsonConvert.SerializeObject(dt);
        //    return str;          
        //} 
        public string GetDisscussionBordDetails(int InstrumentID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            inputParams.Add("@iInstrumentID", InstrumentID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetDiscussionBoardById", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }
        public string AddDisscussionMessages(int InstrumentID, string Message)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iInstrumentID", InstrumentID);
            inputParams.Add("@strMessage", Message);
            inputParams.Add("@iTraderID", globalVaribles.SecUserID);
            var str = sqlDataAccess.ExecuteStoreProcedure("usp_SaveDiscussionMesssages", inputParams);
            return str;
        }
        public string GetPinnedUserList(int InstrumentID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iInstrumentID", InstrumentID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_UserByInstrumentIdForPerformanceSummary", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        string baseUrl = ConfigurationManager.AppSettings["BaseUrl"].ToString();
        public async Task<JsonResult> GetMarketList()
        {
            try
            {
                JObject marketList = new JObject();
                List<mstMarket> marketResult = new List<mstMarket>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/getmarkets");

                    if (resp.IsSuccessStatusCode)
                    {
                        var marketResponse = resp.Content.ReadAsStringAsync().Result;
                        marketList = (JObject)JsonConvert.DeserializeObject(marketResponse);
                        IList<JToken> results = marketList["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            marketResult.Add(new mstMarket
                            {
                                MarketID = Convert.ToInt32(dr["MarketID"]),
                                MarketName = dr["MarketName"].ToString()
                            });
                        }
                        return Json(marketResult, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> GetInstrumentList(int marketId)
        {
            JObject instList = new JObject();
            List<mstInstrument> instDataRes = new List<mstInstrument>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetInstrumentsByMarket?MarketId=" + marketId);

                if (resp.IsSuccessStatusCode)
                {
                    var instResponse = resp.Content.ReadAsStringAsync().Result;
                    instList = (JObject)JsonConvert.DeserializeObject(instResponse);
                    IList<JToken> results = instList["data"].Children().ToList();
                    foreach (JToken dr in results)
                    {
                        instDataRes.Add(new mstInstrument
                        {
                            InstrumentID = Convert.ToInt32(dr["InstrumentID"]),
                            InstrumentsName = dr["InstrumentsName"].ToString()
                        });
                    }
                    return Json(instDataRes, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                }

            }
        }

        public JsonResult GetMarketsByUserId()
        {
            List<mstMarket> MarketList = new List<mstMarket>();
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecApplicationUserID);
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalProviderMarketsByUserId", inputParams);
            foreach (DataRow dr in result.Rows)
            {
                MarketList.Add(new mstMarket
                {
                    MarketID = Convert.ToInt32(dr["MarketID"]),
                    MarketName = dr["MarketName"].ToString()
                });
            }

            return Json(MarketList, JsonRequestBehavior.AllowGet);
        }


        public string GetSignalProviderForFollowUserList()
        {

            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalProviderForFollowUserList", inputParams);
            //foreach (DataRow dr in result.Rows)
            //{
            //    .Add(new mstMarket
            //    {
            //         UserID = Convert.ToInt32(dr["UserID"]),
            //        MarketName = dr["MarketName"].ToString()
            //    });
            //}

            // return Json(result, JsonRequestBehavior.AllowGet);
            ViewBag.UserList = new SelectList(result.ToString(), "UserID", "UserName");
            string str = JsonConvert.SerializeObject(result);
            return str;
        }

        public async Task<JsonResult> GetAllSignalType()
        {
            try
            {
                JObject signalTypeList = new JObject();
                List<mstSignalType> signalListResult = new List<mstSignalType>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/getallsignaltypes");

                    if (resp.IsSuccessStatusCode)
                    {
                        var signalTypeResponse = resp.Content.ReadAsStringAsync().Result;
                        signalTypeList = (JObject)JsonConvert.DeserializeObject(signalTypeResponse);
                        IList<JToken> results = signalTypeList["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            signalListResult.Add(new mstSignalType
                            {
                                SignalTypesID = Convert.ToInt32(dr["SignalTypesID"]),
                                Stype = dr["Stype"].ToString()
                            });
                        }
                        return Json(signalListResult, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> GetAllRiskType()
        {
            try
            {
                JObject riskTypes = new JObject();
                List<mstRisk> riskList = new List<mstRisk>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    //new Uri(baseUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/getallrisks");

                    if (resp.IsSuccessStatusCode)
                    {
                        var risksResponse = resp.Content.ReadAsStringAsync().Result;
                        riskTypes = (JObject)JsonConvert.DeserializeObject(risksResponse);
                        IList<JToken> results = riskTypes["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            riskList.Add(new mstRisk
                            {
                                RiskID = Convert.ToInt32(dr["RiskID"]),
                                RiskName = dr["RiskName"].ToString()
                            });
                        }
                        return Json(riskList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> GetReliabilityType()
        {
            try
            {
                JObject relTypes = new JObject();
                List<mstReliability> realiList = new List<mstReliability>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetAllReliability");

                    if (resp.IsSuccessStatusCode)
                    {
                        var Response = resp.Content.ReadAsStringAsync().Result;
                        relTypes = (JObject)JsonConvert.DeserializeObject(Response);
                        IList<JToken> results = relTypes["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            realiList.Add(new mstReliability
                            {
                                ReliabilityID = Convert.ToInt32(dr["ReliabilityID"]),
                                Symbol = dr["Symbol"].ToString()
                            });
                        }
                        return Json(realiList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public async Task<JsonResult> GetValidityPeriods()
        {
            try
            {
                JObject relTypes = new JObject();
                List<mstValidity> validityList = new List<mstValidity>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetAllPeriods");

                    if (resp.IsSuccessStatusCode)
                    {
                        var Response = resp.Content.ReadAsStringAsync().Result;
                        relTypes = (JObject)JsonConvert.DeserializeObject(Response);
                        IList<JToken> results = relTypes["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            validityList.Add(new mstValidity
                            {
                                ValidityID = Convert.ToInt32(dr["ValidityID"]),
                                PeriodUnit = dr["Period"].ToString()
                            });
                        }
                        return Json(validityList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public async Task<JsonResult> GetTimeFrame()
        {
            try
            {
                JObject relTypes = new JObject();
                List<mstTimeFrame> timeFrameList = new List<mstTimeFrame>();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("serviceproviders/GetAllTimeFrames");

                    if (resp.IsSuccessStatusCode)
                    {
                        var Response = resp.Content.ReadAsStringAsync().Result;
                        relTypes = (JObject)JsonConvert.DeserializeObject(Response);
                        IList<JToken> results = relTypes["data"].Children().ToList();
                        foreach (JToken dr in results)
                        {
                            timeFrameList.Add(new mstTimeFrame
                            {
                                TimeFrameID = Convert.ToInt32(dr["TimeFrameID"]),
                                PeriodUnit = dr["Period"].ToString()
                            });
                        }
                        return Json(timeFrameList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public async Task<JsonResult> GetLivePriceData(int InstrumentID)
        {
            try
            {
                JObject relTypes = new JObject();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage resp = await client.GetAsync("signalmanagement/GetInstrumentDetailsByInstrumentID?InstrumentID=" + InstrumentID);

                    if (resp.IsSuccessStatusCode)
                    {
                        var Response = resp.Content.ReadAsStringAsync().Result;
                        var resvalues = JObject.Parse(Response);
                        decimal liveData = Convert.ToDecimal(resvalues["data"]["PriceMid"]);
                        return Json(liveData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                var ExceptionMsg = ex.Message;
                return Json(new { success = true, result = ExceptionMsg }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> SaveSignal(string result)
        {
            try
            {
                sqlDataAccess = new InfoFreightSystem.SQLDataAccess();
                inputParams = new Hashtable();
                string userName = Session["UserId"].ToString();
                inputParams.Add("@strApplicationUserLoginName", userName);
                var applicationUserId = sqlDataAccess.ExecuteStoreProcedure("usp_GetApplicationUserID", inputParams);

                dynamic obj = JsonConvert.DeserializeObject(result);
                StringBuilder sb = new StringBuilder();
                sb.Append("?InstrumentId=" + obj.InstrumentId + "&StlD=" + obj.SignalType);
                sb.Append("&SigProID=" + globalVaribles.SecUserID + "&BuySellID=" + obj.BuySell + "&StrikePrice=" + obj.StrikePrice);
                sb.Append("&TargetPrice=" + obj.TakeProfit + "&StopLoss=" + obj.StopLoss);
                sb.Append("&InstrumentPrice=" + obj.LivePrice + "&DirectionCheck=" + string.Empty);
                sb.Append("&strComments=" + obj.Comment + "&MINTimeFrameID=" + obj.TimeMinFrame);
                sb.Append("&MaxTimeFrameID=" + obj.timeMaxFrame + "&MaxTimeFrame=" + "2019-12-26 18:55:46.553");
                sb.Append("&MinValidityID=" + obj.ValidityMin + "&MaxValidityID=" + obj.ValidityMax);
                sb.Append("&MaxValidityDate=" + "2019-12-26 18:55:46.553");
                sb.Append("&ReliabilityID=" + obj.Reliability);
                sb.Append("&RiskID=" + obj.RiskType + "&ProcessStatusID=" + 1 + "&SuccessStatusID=" + 1);
                sb.Append("&ClosingPips=" + "0.0" + "&ClosingPercent=" + 0 + "&EntryDate=" + DateTime.Now);
                sb.Append("&ClosingDate=" + "2019-12-26 18:55:46.553");
                sb.Append("&UpdatedBy=" + userName + "&LivePrice=" + obj.LivePrice);

                JObject relTypes = new JObject();
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(ConfigurationManager.AppSettings["BaseUrl"].ToString());
                    // client.BaseAddress = new Uri("http://localhost:53743/api/signalmanagement/");
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage resp = await client.GetAsync("signalmanagement/savenewsignal" + sb.ToString());
                    if (resp.IsSuccessStatusCode)
                    {
                        var Response = resp.Content.ReadAsStringAsync().Result;
                        dynamic data = JsonConvert.DeserializeObject(Response);
                        var g = data.data;
                        int storedValue = g.ToObject<int>();
                        relTypes = (JObject)JsonConvert.DeserializeObject(Response);
                        return Json(new { success = true, result = storedValue }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            catch (Exception ex)
            {
                string errorMsg = ex.Message;
                return Json(new { success = true, result = "Fail" }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetSignalLibrary()
        {
            inputParams = new Hashtable();
            inputParams.Add("@SigProID", globalVaribles.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SP_SignalLibrary", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetSignalDashbord()
        {
            inputParams = new Hashtable();
            inputParams.Add("@SigProID", globalVaribles.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalDashboard", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        //public JsonResult GetLiveDataFeed(string[] instrumentName)
        //{
        //    try
        //    {
        //        string str = "";        
        //        JArray a = JArray.Parse(instrumentName[0]);
        //        int[] ids = a.Select(i => (int)i).ToArray();
        //        str = ids[0].ToString();
        //        for (int i= 1; i < ids.Length; i++)
        //        {
        //            str +=','+ ids[i].ToString();
        //        }
        //        using (var connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString))
        //        {
        //            connection.Open();
        //            using (SqlCommand command = new SqlCommand(@"SELECT  InstrumentName, PriceMid FROM InstrumentPriceLive where InstrumentID IN (" + str +')', connection))
        //            {
        //                command.Notification = null;
        //                SqlDependency dependency = new SqlDependency(command);
        //                dependency.OnChange += new OnChangeEventHandler(dependency_OnChange);

        //                if (connection.State == ConnectionState.Closed)
        //                    connection.Open();

        //                SqlDataReader reader = command.ExecuteReader();

        //                var listCus = reader.Cast<IDataRecord>()
        //                        .Select(x => new
        //                        {
        //                            InstrumentName = (string)x["InstrumentName"],
        //                            PriceMid = (decimal)x["PriceMid"]
        //                        }).ToList();
        //                connection.Close();
        //                return Json(new { listCus = listCus }, JsonRequestBehavior.AllowGet);
        //            }

        //        }
        //    }
        //    catch(Exception ex)
        //    {
        //        var msg = ex.Message;
        //        return Json(new { Message = msg }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        //private void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        //{
        //    CusHub.Show();
        //}
        public string GetClosedSignals()
        {
            inputParams = new Hashtable();
            inputParams.Add("@SigProID", globalVaribles.SecUserID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalClosed", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetTimeFrameString(int minTimeFrameId, int maxTimeFrameId)
        {
            inputParams = new Hashtable();
            inputParams.Add("@minTimeFrameID", minTimeFrameId);
            inputParams.Add("@maxTimeFrameID", maxTimeFrameId);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_TimeFramePeriodUnits", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetPinnedCount(int SigID)
        {
            string str = "";
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SigID", SigID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_PinnedCountForSignal", inputParam);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            str = JsonConvert.SerializeObject(dt);
            //  int count = Convert.ToInt32(str);
            return str;
        }
        public string GetViewSignalCount(int SigID)
        {
            string str = "";
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SigID", SigID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_ViewCountForSignal", inputParam);
            if (dt.Rows.Count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetSignalProviderInstrumentSuccessRate(int sigProID, int instrumentID)
        {
            string str = "";
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SigProID", sigProID);
            inputParam.Add("@InstrumentID", instrumentID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderInstrumentSuccessRate", inputParam);
            int count = dt.Rows.Count;
            if (count == 0)
            {
                str = "0";
            }
            else
            {
                str = JsonConvert.SerializeObject(dt);
            }
            return str;
        }
        public string GetSignalDetails(int signalsID)
        {
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@SignalsID", signalsID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalDetails", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public int NumberOfTradersForInstrument(int instrumentID)
        {
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@InstrumentID", instrumentID);
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_NumberOfTradersForInstrument", inputParam);
            int cnt = Convert.ToInt32(dt.Rows[0]["TraderCount"]);
            return cnt;
        }
        public decimal GetMultiplyingFactor(int instrumentID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@InstrumentID", instrumentID);
            var applicationUserId = sqlDataAccess.ExecuteStoreProcedure("usp_GetMultiplierFactorByID", inputParams);
            decimal multiplier = Convert.ToDecimal(applicationUserId);
            ViewBag.Multiplier = multiplier;
            return multiplier;
        }
        public string GetSignalProviderActivityPerformanceForPeriod()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@SigProID", globalVaribles.SecUserID);
            inputParams.Add("@MarketID", 1);
            inputParams.Add("@PeriodID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderActivityPerformanceForPeriod", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SignalProviderActivityPinnedCount()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DateTime startDate = Convert.ToDateTime("2019-12-05 00:00");
            DateTime validTill = DateTime.Now;
            inputParams.Add("@SigProID", globalVaribles.SecUserID);
            inputParams.Add("@MarketID", 1);
            inputParams.Add("@StartFrom", startDate);
            inputParams.Add("@ValidTill", validTill);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderActivityPinnedCount", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SignalProviderActivityProcessStatusCount()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            DateTime startDate = Convert.ToDateTime("2019-12-05 00:00");
            DateTime validTill = DateTime.Now;
            inputParams.Add("@SigProID", 240);
            inputParams.Add("@MarketID", 1);
            inputParams.Add("@StartFrom", startDate);
            inputParams.Add("@ValidTill", validTill);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderActivityProcessStatusCount", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public ActionResult Performance()
        {

            List<SelectListItem> items = new List<SelectListItem>();
            sqlDataAccess = new SQLDataAccess();
            DataTable periodDT = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPeriod");
            if (periodDT.Rows.Count > 0 && periodDT != null)
            {
                foreach (DataRow dr in periodDT.Rows)
                {
                    items.Add(new SelectListItem
                    {
                        Text = "Period " + dr["SignalPerformanceEarningsPeriodID"].ToString(),
                        Value = dr["SignalPerformanceEarningsPeriodID"].ToString()
                    });
                }
            }
            ViewBag.Period = items;


            return View();
        }
        public string GetPerformanceData()
        {
            Int32 marketId = 1;
            sqlDataAccess = new SQLDataAccess();
            inputParams = new Hashtable();
            inputParams.Add("@SigProID", globalVaribles.SecApplicationUserID);
            inputParams.Add("@iMarketID", marketId);
            var response = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_Get_SignalProviderPerformanceDetails", inputParams);
            string str = JsonConvert.SerializeObject(response);
            return str;

        }
        public string GetSignalProviderPeroid1(int PeriodOne)
        {

            // DataSet dt = new DataSet();
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iPeriod1", PeriodOne);
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SP_ComparePerformanceP1", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetSignalProviderPeroid2(int PeriodTwo)
        {
            // DataSet dt = new DataSet();
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iPeriod2", PeriodTwo);
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SP_ComparePerformanceP2", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #region Preferences  
        public ActionResult Preferences()
        {
            return View();
        }
        public string GetSignalProviderMarket()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecApplicationUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalProviderMarketsByUserId", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string AddMarketForUser(int MarketID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iSigProID", globalVaribles.SecApplicationUserID);
            inputParams.Add("@iMarketID", MarketID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_SaveSignalProviderMarkets", inputParams);
            return dt;

        }
        public string DeleteMarketByUserId(int SignalProviderMarketsID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iSignalProviderMarketsID", SignalProviderMarketsID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteSignalProviderMarkets", inputParams);
            return dt;
        }

        public string AddInstrumentForUser(int InstrumentID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iSigProID", globalVaribles.SecApplicationUserID);
            inputParams.Add("@iInstrumentID", InstrumentID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_SaveSignalProviderInstruments", inputParams);
            return dt;

        }

        public string AddSignalProviderFollow(int SigProFollowerID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iSigProID", globalVaribles.SecUserID);
            inputParams.Add("@iSigProFollowerID", SigProFollowerID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_SaveSignalProviderFollowSignalProvider", inputParams);
            return dt;

        }
        public string GetSignalProviderInstrument()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecApplicationUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalProviderInstrumentsByUserId", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string DeleteSignalProviderInstrumentByUser(int SignalProviderInstrumentsID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iSignalProviderInstrumentsID", SignalProviderInstrumentsID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteSignalProviderInstrument", inputParams);
            return dt;
        }
        public string GetSignalProviderFollowDetails()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalProviderFollowSignalProvidersByUserId", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }

        public string UnfollowSignalProvider(int SignalProviderFollowSignalProvidersID)
        {
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iSignalProviderFollowSignalProvidersID", SignalProviderFollowSignalProvidersID);
            var dt = sqlDataAccess.ExecuteStoreProcedure("usp_DeleteSignalProviderFollowUser", inputParams);
            return dt;
        }
        #endregion

        #endregion
        #region Transactions
        public ActionResult Earnings()
        {
            return View();
        }
        public string GetEarningDetails()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", Convert.ToInt32(globalVaribles.SecUserID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetSignalProviderEarningsList", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public ActionResult Cashout()
        {
            return View();
        }

        public string AddCashoutAmountDeatils(decimal RequestAmout, decimal CurrentWallentAmount)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@decRequestAmout", Convert.ToDecimal(RequestAmout));
            inputParam.Add("@decCurrentWallentAmount", Convert.ToDecimal(CurrentWallentAmount));
            inputParam.Add("@iUserID", Convert.ToInt32(globalVaribles.SecUserID));
            inputParam.Add("@iRoleID", globalVaribles.RoleID);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveUserCashoutEarningRequestDetails", inputParam);
            return "";
        }
        public string GetCashout()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            inputParams.Add("@iRoleID", globalVaribles.RoleID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserEarningCashoutRequest", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public string GetWalletAmountByID()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            inputParams.Add("@iRoleID", globalVaribles.RoleID);
            var Result = sqlDataAccess.ExecuteStoreProcedure("usp_GetUserWalletBalance", inputParams);
            // string str = JsonConvert.SerializeObject(dt);
            return Result;
        }
        public ActionResult PlanPayments()
        {
            return View();
        }
        public string GetPlanPayment()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", globalVaribles.SecUserID);
            var Result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetPlanPaymentList", inputParams);
            string str = JsonConvert.SerializeObject(Result);
            return str;
        }
        #endregion
        #region Success Story
        public ActionResult SuccessStory()
        {
            return View();
        }
        public string GetSuccessStory()
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iMarketID", 1);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SignalProviderSuccessStory", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;

        }
        public string SearchSuccessStorySignalProvider(int MarketID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iMarketID", MarketID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SignalProviderSuccessStory", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetSuccessStoryTrader(int MarketID)
        {
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iMarketID", MarketID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_SignalProviderSuccessStory", inputParams);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region Support
        public ActionResult GetTicketSubjectList()
        {
            List<mstTicketSubject> SubjectList = new List<mstTicketSubject>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketSubjectList");
            foreach (DataRow dr in dt.Rows)
            {
                SubjectList.Add(new mstTicketSubject
                {
                    TicketSubjectID = Convert.ToInt32(dr["TicketSubjectID"]),
                    Subject = dr["Subject"].ToString()
                });
            }
            return Json(new { success = true, TicketSubjectList = SubjectList }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Tickets()
        {
            return View();
        }
        public string AddTicketStatus(string Subject, string Message)
        {
            var fileName = "";
            var path = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                file.SaveAs(path);
            }
            byte[] theBytes = Encoding.UTF8.GetBytes(fileName);
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iFromID", Convert.ToInt32(globalVaribles.SecUserID));
            inputParam.Add("@iToID", Convert.ToInt32(1));
            inputParam.Add("@iTicketSubjectID", Subject);
            inputParam.Add("@strMessage", Message);
            inputParam.Add("@iTicketStatusID", 1);
            inputParam.Add("@imgChooseScreen", theBytes);
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveTicketDetails", inputParam);
            return res;
        }
        public ActionResult GetTicketImageById(int TicketID)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iID", TicketID);
                inputParams.Add("@strScreen", "Ticket");
                sqlDataAccess = new SQLDataAccess();
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserFile", inputParams);
                string DocArray = Encoding.ASCII.GetString((byte[])dt.Rows[0]["ChooseScreen"]);
                return Json(new { success = true, TicketImage = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, TicketImage = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public string AddTicketReplay(int TicketsID, string Message)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketID", Convert.ToInt32(TicketsID));
            inputParam.Add("@strMessage", Convert.ToString(Message));
            inputParam.Add("@iTicketStatusID", Convert.ToInt32(1));
            inputParam.Add("@iResponsebyID", Convert.ToInt32(globalVaribles.SecUserID));
            string res = sqlDataAccess.ExecuteStoreProcedure("usp_SaveTicketResponses", inputParam);
            return res;
        }

        public string GetResponseByTicket(int TicketsID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketID", Convert.ToInt32(TicketsID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketResponsesById", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string GetTicketResponseMsgByID(int TicketResponsesID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iTicketResponsesID", Convert.ToInt32(TicketResponsesID));
            string Result = sqlDataAccess.ExecuteStoreProcedure("usp_GetResponsesMsgById", inputParam);
            // string str = JsonConvert.SerializeObject(dt);
            return Result;
        }
        public JsonResult GetTicketStatusList()
        {

            List<mstTicketStatu> TickestStatusList = new List<mstTicketStatu>();
            sqlDataAccess = new SQLDataAccess();
            var result = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTicketStatusList");

            foreach (DataRow dr in result.Rows)
            {
                TickestStatusList.Add(new mstTicketStatu
                {
                    TicketStatusID = Convert.ToInt32(dr["TicketStatusID"]),
                    TicketStatus = dr["TicketStatus"].ToString()
                });
            }
            return Json(TickestStatusList, JsonRequestBehavior.AllowGet);
        }
        public string GetAllTicketStatusDetails()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iSecUserID", Convert.ToInt32(globalVaribles.SecUserID));
            inputParam.Add("@sdtFromDate", string.Empty);
            inputParam.Add("@sdtToDate", string.Empty);
            inputParam.Add("@iTicketStatusID", 0);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTraderTicketsDetails", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        public string SearchTicketsDetails(string FromData, string TODate, string TicketStatusID)
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iSecUserID", Convert.ToInt32(globalVaribles.SecUserID));
            inputParam.Add("@sdtFromDate", FromData);
            inputParam.Add("@sdtToDate", TODate);
            inputParam.Add("@iTicketStatusID", TicketStatusID);
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetTraderTicketsDetails", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }

        public ActionResult FAQs()
        {
            return View();
        }
        public ActionResult Feedback()
        {
            return View();
        }
        public ActionResult GetFeedbackSubjectList()
        {
            List<mstFeedbackSubject> FeedbackSubjectList = new List<mstFeedbackSubject>();
            sqlDataAccess = new SQLDataAccess();
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetFeedbackSubjectList");
            foreach (DataRow dr in dt.Rows)
            {
                FeedbackSubjectList.Add(new mstFeedbackSubject
                {
                    FeedbackSubjectID = Convert.ToInt32(dr["FeedbackSubjectID"]),
                    Subject = dr["Subject"].ToString()
                });
            }
            return Json(new { success = true, FeedbackSubjectList = FeedbackSubjectList }, JsonRequestBehavior.AllowGet);
        }
        public string AddFeedback(string comments, string subject)
        {
            var fileName = "";
            var path = "";
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                fileName = Path.GetFileName(file.FileName);
                path = Path.Combine(Server.MapPath("~/Content/CSS/img"), fileName);
                file.SaveAs(path);
            }
            byte[] theBytes = Encoding.UTF8.GetBytes(fileName);

            //  dynamic obj = JsonConvert.DeserializeObject(result);
            inputParams = new Hashtable();
            sqlDataAccess = new SQLDataAccess();
            inputParams.Add("@iUserID", Convert.ToInt32(globalVaribles.SecUserID));
            inputParams.Add("@iFeedbackSubjectID", Convert.ToInt32(subject));
            inputParams.Add("@strFeedbackComment", Convert.ToString(comments));
            inputParams.Add("@imgFeedbackFile", theBytes);
            string resp = sqlDataAccess.ExecuteStoreProcedure("usp_SaveFeedback", inputParams);
            return "";
        }
        public ActionResult GetImageById(int FeedbackID)
        {
            try
            {
                inputParams = new Hashtable();
                inputParams.Add("@iID", FeedbackID);
                inputParams.Add("@strScreen", "Feedback");
                sqlDataAccess = new SQLDataAccess();
                // String[] DocArray = new String[1];
                var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetUserFile", inputParams);
                string DocArray = Encoding.ASCII.GetString((byte[])dt.Rows[0]["FeedbackFile"]);


                return Json(new { success = true, FeedBackImage = DocArray }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { success = true, FeedBackImage = "Fail" }, JsonRequestBehavior.AllowGet);
            }

        }
        public string GetAllFeedback()
        {
            sqlDataAccess = new SQLDataAccess();
            Hashtable inputParam = new Hashtable();
            inputParam.Add("@iUserID", Convert.ToInt32(globalVaribles.SecUserID));
            var dt = sqlDataAccess.GetDatatableExecuteStoreProcedure("usp_GetmstFeedback", inputParam);
            string str = JsonConvert.SerializeObject(dt);
            return str;
        }
        #endregion

        #region Take A Tour
        public ActionResult TakeATour()
        {
            return View();
        }
        #endregion
        #region SiteMap
        public ActionResult SiteMap()
        {
            return View();
        }
        #endregion
    }
}