﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;


namespace OrderWorkAPI.Services
{
    /// <summary>
    /// class EmailSimple 
    /// </summary>
    public class EmailSimple
    {

        #region Private Fields

        private static string _fromAddress;
        private static string _strSmtpClient;
        private static string _userId;
        private static string _password;
        private static string _smtpPort;
        private static bool _bEnableSsl;
        private static List<string> _listWebConfigCCs;
        private static List<string> _listWebConfigBCCs;


        #endregion


        #region Send Email Method
        /// <summary>
        /// Configurations the send email.
        /// </summary>
        /// <param name="mailTo">The mail to.</param>
        /// <param name="mailSubject">The mail subject.</param>
        /// <param name="mailBody">The mail body.</param>
        /// <returns></returns>
        public bool ConfigSendEmail(string mailTo, string mailSubject, string mailBody, List<string> _listCCs = null,
            List<string> _listBCCs = null, System.Net.Mail.Attachment attachment = null)
        {
            bool mailStatus = false;
            GetMailData();
            //Log.Info("45->after get mail data");
            dynamic mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(_fromAddress);
            mailMessage.To.Add(mailTo);
            if (attachment != null)
            {
                // Attachment data = new Attachment(attachment.Name, MediaTypeNames.Application.Octet);
                // Add time stamp information for the file.
                //ContentDisposition disposition = attachment.ContentDisposition;
                //disposition.CreationDate = System.IO.File.GetCreationTime(attachment);
                //disposition.ModificationDate = System.IO.File.GetLastWriteTime(attachment);
                //disposition.ReadDate = System.IO.File.GetLastAccessTime(attachment);
                mailMessage.Attachments.Add(attachment);
            }
            var listCCs = _listCCs ?? _listWebConfigCCs;
            var listBCCs = _listBCCs ?? _listWebConfigBCCs;

            if (listCCs.Count != 0)
            {
                foreach (var email in listCCs)
                {
                    mailMessage.CC.Add(email);
                }
            }
            //Log.Info("59->after get mail data");
            if (listBCCs.Count != 0)
            {

                foreach (var email in listBCCs)
                {
                    mailMessage.Bcc.Add(email);
                }
            }
           //// Log.Info("68->after get mail data");
            //mailMessage.CC.Add("portaladmin@empowereduk.com"); 
            //            mailMessage.Bcc.Add("fi@iniquus.com");
            mailMessage.Subject = mailSubject;
            mailMessage.IsBodyHtml = true;
            mailMessage.Body = mailBody;

            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = "smtp.gmail.com";
            smtpClient.EnableSsl = true;//_bEnableSsl;
            smtpClient.Port = Int32.Parse(_smtpPort);
            smtpClient.Credentials = new System.Net.NetworkCredential(_userId, _password);
          ////  Log.Info("80->");
            try
            {
                try
                {
                    smtpClient.Send(mailMessage);
               ///     Log.Info("86->");
                    mailStatus = true;
                }
                catch (Exception ex)
                {
               ///     Log.Error(ex);
                }
            }
            catch (SmtpFailedRecipientsException ex)
            {
                for (int i = 0; i <= ex.InnerExceptions.Length; i++)
                {
                    SmtpStatusCode status = ex.StatusCode;
                    if ((status == SmtpStatusCode.MailboxBusy) | (status == SmtpStatusCode.MailboxUnavailable))
                    {
                        System.Threading.Thread.Sleep(5000);
                        smtpClient.Send(mailMessage);
                    }
                }
               ///// Log.Error(ex);
            }
            return mailStatus;
        }

        #endregion

        #region Get Email provider data From Web.config file

        /// <summary>
        /// Gets the mail data.
        /// </summary>
        private static void GetMailData()
        {
            _fromAddress = System.Configuration.ConfigurationManager.AppSettings.Get("FromAddress");
            _strSmtpClient = System.Configuration.ConfigurationManager.AppSettings.Get("SmtpClient");
            _userId = System.Configuration.ConfigurationManager.AppSettings.Get("UserID");
            _password = System.Configuration.ConfigurationManager.AppSettings.Get("Password");
            _smtpPort = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPPort");
            _listWebConfigCCs = System.Configuration.ConfigurationManager.AppSettings.Get("emailCCs").Split(',').ToList();
            _listWebConfigBCCs = System.Configuration.ConfigurationManager.AppSettings.Get("emailBCCs").Split(',').ToList();
            if ((System.Configuration.ConfigurationManager.AppSettings.Get("EnableSSL") == null))
            {
            }
            else
            {
                _bEnableSsl = (System.Configuration.ConfigurationManager.AppSettings.Get("EnableSSL").ToUpper() ==
                              "YES");
            }
        }

        #endregion
    }
}