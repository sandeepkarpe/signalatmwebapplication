﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SignalATM.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
           // SqlDependency.Start(ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString);

        }
        protected void Application_End()
        {
           // SqlDependency.Stop(ConfigurationManager.ConnectionStrings["SignalATMEntitiesNew"].ConnectionString);
        }
    }
}
