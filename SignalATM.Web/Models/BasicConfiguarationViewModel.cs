﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SignalATM.Web.Models
{
    public class BasicConfiguarationViewModel
    {
        public List<mstInstrument> InstrumentList { get; set; }
        public List<mstMarket> MarketList { get; set; }
    }
}