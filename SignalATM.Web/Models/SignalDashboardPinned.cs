//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SignalATM.Web.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SignalDashboardPinned
    {
        public int SignalDashboardPinnedID { get; set; }
        public int TraderID { get; set; }
        public int SigID { get; set; }
        public string SignalUpdates { get; set; }
        public System.DateTime EntryDate { get; set; }
    
        public virtual Signal Signal { get; set; }
    }
}
