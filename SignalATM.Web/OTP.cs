﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace OrderWorkAPI.Services
{
    public class otp
    {     
        public static string GenerateOtp()
        {
            try
            {
                // uppercase
                const string upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                var otpUpperCase = string.Empty;
                for (var i = 0; i <= 2; i++)
                {
                    string character;
                    do
                    {
                        var index = new Random().Next(0, upperCase.Length);
                        character = upperCase.ToCharArray()[index].ToString();
                    } while (otpUpperCase.IndexOf(character, StringComparison.Ordinal) != -1);
                    otpUpperCase += character;
                }

                // lower case
                const string lowerCase = "abcdefghijklmnopqrstuvwxyz";
                var otpLowerCase = string.Empty;
                for (var i = 0; i <= 2; i++)
                {
                    string character;
                    do
                    {
                        var index = new Random().Next(0, lowerCase.Length);
                        character = lowerCase.ToCharArray()[index].ToString();
                    } while (otpLowerCase.IndexOf(character, StringComparison.Ordinal) != -1);
                    otpLowerCase += character;
                }

                // digits
                const string digit = "1234567890";
                var otpDigit = string.Empty;
                for (var i = 0; i <= 1; i++)
                {
                    string character;
                    do
                    {
                        var index = new Random().Next(0, digit.Length);
                        character = digit.ToCharArray()[index].ToString();
                    } while (otpDigit.IndexOf(character, StringComparison.Ordinal) != -1);
                    otpDigit += character;
                }

                // final random otp           
                var inputString = Convert.ToString(otpUpperCase + otpLowerCase + otpDigit);
                var randomNo = new Random();
                var finalOtp = "";
                while (inputString.Length > 0)
                {
                    var i = randomNo.Next(0, inputString.Length);
                    finalOtp += inputString.Substring(i, 1);
                    inputString = inputString.Remove(i, 1);
                }
                return finalOtp;
            }
            catch (Exception ex)
            {
                //Log.Error(ex);
                return null;
            }
        }
    }
}